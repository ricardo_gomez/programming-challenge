import Hapi from '@hapi/hapi';
import { makeDb, startDatabase } from './database';
import dotenv from 'dotenv';

const init = async () => {
  dotenv.config();
  const db = makeDb();

  const server = Hapi.server({
    port: 4000,
    host: 'localhost',
    routes: {
      cors: {
        origin: ['*']
      }
    }
  });

  server.route({
    method: 'GET',
    path: '/tasks',
    handler: async (r, h) => {
      try {
        const { rows } = await db.raw('select * from tasks');
        return h.response(rows).code(200)
      } catch (error) {
        console.error(error);
        return h.response().code(500)        
      }
    } 
  });

  server.route({
    method: 'POST',
    path: '/submit-task',
    handler: async (r, h) => {
      try {
        const task = await db('tasks')
          .returning('*')
          .insert({ content: r.payload });

        return h.response(task).code(200);
      } catch (error) {
        console.error(error);
        return h.response().code(500);
      }
    } 
  });

  server.route({
    method: 'PATCH',
    path: '/update-task/{id}',
    handler: async (r, h) => {
      try {
        await db('tasks')
          .where('tasks_id', r.params.id)
          .update(r.payload);

        return h.response().code(200);
      } catch (error) {
        console.error(error);
        return h.response().code(500);
      }
    } 
  });

  await server.start();
  console.log('Server running on %s', server.info.uri);
};

process.on('unhandledRejection', (err) => {
  console.error(err);
  process.exit(1);
});

init();
startDatabase();
