import { ref } from 'vue';
import api from '../utils/axios';

import type { Task } from '../utils/types';

export const useSubmitTask = () => {
  const state = ref<'loading' | 'failed' | 'loaded'>('loaded');

  const submitTask = (task: string): Promise<Task | undefined> => {
    state.value = 'loading';

    return api.post<Task>('/submit-task', task)
      .then((response) => {
        state.value = 'loaded';
        return response.data;
      })
      .catch((error) => {
        console.error(error);
        state.value = 'failed';
        return undefined;
      });
  };

  return { state, submitTask };
};
