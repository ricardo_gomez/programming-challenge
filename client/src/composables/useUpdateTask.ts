import { ref } from 'vue';
import api from '../utils/axios';

import type { Task } from '../utils/types';

export const useUpdateTask = () => {
  const state = ref<'loading' | 'failed' | 'loaded'>('loading');

  const updateTask = (task: any) => {
    state.value = 'loading';

    api.patch<Task>(`/update-task/${task.tasks_id}`, task)
      .then((response) => {
        state.value = 'loaded';
      })
      .catch((error) => {
        console.error(error);
        state.value = 'failed';
      });
  };

  return { state, updateTask };
};
