export type Task = {
  tasksId: number;
  content: string;
  isComplete: boolean;
  is_complete: number;
  order: number;
};
